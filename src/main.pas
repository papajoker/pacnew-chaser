unit main;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls,
    IpHtml, Ipfilebroker, Process, Graphics, Dialogs, LMessages,
    ComCtrls, StdCtrls, ExtCtrls, Menus, ActnList, LCLType, LCLIntf, Types,
    config, bash, pacnew, backups;

const
    APP_UPDATE = LM_USER + 1;
    APP_EDIT = LM_USER + 2;
    APP_BACKUP_OK = LM_USER + 3;

type

    { TMainWin }

    ThtmlTheme = record
        bg: String[7];
        txt: String[7];
        more: String[7];
        less: String[7];
    end;

    TMainWin = class(TForm)
        A_toggleLevel: TAction;
        A_colorTxt: TAction;
        A_colorBg: TAction;
        A_backup: TAction;
        A_toggleView: TAction;
        A_update: TAction;
        A_close: TAction;
        A_listConfs: TAction;
        A_listConfsModidied: TAction;
        A_man: TAction;
        A_diff: TAction;
        A_edit: TAction;
        A_delete: TAction;
        ActionList1: TActionList;
        ColorDialog: TColorDialog;
        ImageListSmall: TImageList;
        ImageListBig: TImageList;
        Html: TIpHtmlPanel;
        IpFileDataProvider1: TIpFileDataProvider;
        ListFiles: TListView;
        M_sep2: TMenuItem;
        MenuItem10: TMenuItem;
        MenuItem2: TMenuItem;
        MenuItem3: TMenuItem;
        MenuItem4: TMenuItem;
        MenuItem5: TMenuItem;
        MenuItem6: TMenuItem;
        MenuItem7: TMenuItem;
        MenuItem8: TMenuItem;
        MenuItem9: TMenuItem;
        M_sep: TMenuItem;
        M_man: TMenuItem;
        M_diff: TMenuItem;
        M_edit: TMenuItem;
        M_delete: TMenuItem;
        MainPanel: TPanel;
        PopupMenuItem: TPopupMenu;
        PopupMenuApp: TPopupMenu;
        SaveDialog: TSaveDialog;
        Splitter1: TSplitter;
        Status: TStatusBar;
        procedure A_backupExecute(Sender: TObject);
        procedure A_closeExecute(Sender: TObject);
        procedure A_colorBgExecute(Sender: TObject);
        procedure A_colorTxtExecute(Sender: TObject);
        procedure A_listConfsExecute(Sender: TObject);
        procedure A_listConfsModidiedExecute(Sender: TObject);
        procedure A_manExecute(Sender: TObject);
        procedure A_deleteExecute(Sender: TObject);
        procedure A_diffExecute(Sender: TObject);
        procedure A_editExecute(Sender: TObject);
        procedure A_toggleLevelExecute(Sender: TObject);
        procedure A_toggleViewExecute(Sender: TObject);
        procedure A_updateExecute(Sender: TObject);
        procedure EditorDblClick(Sender: TObject);
        procedure EditorDragDrop(Sender, Source: TObject; X, Y: Integer);
        procedure EditorDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
        procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
        procedure FormCreate(Sender: TObject);
        procedure FormDestroy(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure ListFilesContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
        procedure ListFilesDblClick(Sender: TObject);
        procedure ListFilesDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
        procedure ListFilesEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
        procedure ListFilesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
        procedure ListFilesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
        procedure ListFilesSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
        procedure ListFilesUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
        procedure PopupMenuItemPopup(Sender: TObject);
    private
        libDir: String;
        pacnews: TPacnews;
        backupList: TBackupList;
        htmlTheme: ThtmlTheme;
        function getSeletedPacnew: Tpacnew;
        function getHtmlHeader: String;
        function controlBackupOk(notify: Boolean = False): Boolean;
        procedure EditFiles();
    public
        config: Tconfig;
        function getEditor: String;
        procedure desktopNotify(message: String; long: Integer = 3);
        procedure WndProc(var Msg: TLMessage); override;
    published
        property pacnew: Tpacnew read getSeletedPacnew;
    end;

var
    MainWin: TMainWin;


function journald_print(priority: Longint; msg: PChar{; pid:longint}): Longint; cdecl; external 'libsystemd' Name 'sd_journal_print';

implementation

uses DateUtils;

const
    backupFile = '/tmp/backup-config-system.tar.gz';

resourcestring
    msg_info_save = 'for save all my config before editing';
    msg_msg_save = 'save my config before editing !';
    msg_err_noeditor = 'No editor found';
    msg_info_status = 'find/compare .pacnew';
    msg_quest_remove = 'Remove pacnew';
    msg_info_removed = 'removed';
    msg_html_modfiles = 'Modified backup Files';
    msg_html_backupfiles = 'Backup files';
    msg_mnu_diff = 'Difference:';
    msg_info_modified = 'pacnew modified';
    msg_info_file_modified = 'active file modified';
    msg_info_confuse = '2 files modified ?';
    msg_backup_files = '.bak file created in';
    msg_no_pacnew = 'No pacnew found';
    msg_override = 'You can prevent pacnew with';
    msg_modalmode = 'Close editor for continue';
    msg_badfile = 'Cannot edit this file';

{$R *.lfm}

{ TMainWin }

function SHtmlColorToColor(s: String; Default: TColor): TColor;
var
    N1, N2, N3: Integer;
    i, Len:     Integer;

    function IsCharWord(ch: Char): Boolean;
    begin
        Result := ch in ['a'..'z', 'A'..'Z', '_', '0'..'9'];
    end;

    function IsCharHex(ch: Char): Boolean;
    begin
        Result := ch in ['0'..'9', 'a'..'f', 'A'..'F'];
    end;

begin
    Result := Default;
    Len := 0;
    if (s <> '') and (s[1] = '#') then
        Delete(s, 1, 1);
    if (s = '') then
        exit;

    //delete after first nonword char
    i := 1;
    while (i <= Length(s)) and IsCharWord(s[i]) do
        Inc(i);
    Delete(s, i, Maxint);

    //allow only #rgb, #rrggbb
    Len := Length(s);
    if (Len <> 3) and (Len <> 6) then
        exit;

    for i := 1 to Len do
        if not IsCharHex(s[i]) then
            exit;

    if Len = 6 then
    begin
        N1 := StrToInt('$' + Copy(s, 1, 2));
        N2 := StrToInt('$' + Copy(s, 3, 2));
        N3 := StrToInt('$' + Copy(s, 5, 2));
    end
    else
    begin
        N1 := StrToInt('$' + s[1] + s[1]);
        N2 := StrToInt('$' + s[2] + s[2]);
        N3 := StrToInt('$' + s[3] + s[3]);
    end;

    Result := RGBToColor(N1, N2, N3);
end;

procedure TMainWin.FormCreate(Sender: TObject);
const
    libSys = '/usr/lib/pacnew-chaser';
var
    i: Integer;

    function getColor(Caption: String; default: TColor = clDefault): TColor;
    var
        Value: String = '';
    begin
        Result := default;
        Value := Config.ReadValue('COLORS', Caption, ColorToString(default));
        if (copy(Value, 1, 1) = '#') then
        begin
            Result := SHtmlColorToColor(Value, default);
        end;
    end;

begin
    Config := Tconfig.Create();
    writeln('Tconfig.programName: ', Tconfig.programName);
    writeln('Tconfig.version: ', Tconfig.version);
    writeln('Tconfig.ini: ', Tconfig.IniEtcFile);
    writeln('Tconfig.ini: ', Tconfig.IniHomeFile);

    writeln('hook pacman: ', Config.readValue('CONF', 'hook', 0));


    i := Config.ReadValue('POSITION', 'h', 0);
    if i > 0 then
        self.Height := i;
    i := Config.ReadValue('POSITION', 'w', 0);
    if i > 0 then
        self.Width := i;
    i := Config.ReadValue('POSITION', 'x', -1);
    if i > 0 then
        self.Left := i;
    i := Config.ReadValue('POSITION', 'y', -1);
    if i > 0 then
        self.Top := i;


    ListFiles.Items.Clear;
    pacnews := TPacnews.Create();
    backupList := TBackupList.Create();
    //Splitter1.SetSplitterPosition(MainPanel.ClientHeight);
    Splitter1.SetSplitterPosition(round(MainPanel.ClientHeight / 2));

    ListFiles.Color := clForm;
    ListFiles.Font.Color := ClWindowText;
    Splitter1.Color := clDefault;
    //Editor.Color := clBtnFace;
     {$IFDEF LCLGTK3}
    Status.Color := ClBtnFace;
    Status.Font.Color := ClWindowText;
     {$ENDIF}

    ListFiles.Color := getColor('files_bg', clForm);
    //MainPanel.Color := getColor('files_bg',clForm);
    ListFiles.Font.Color := getColor('files_font', ClWindowText);
    Status.Color := getColor('status_bg', clInactiveBorder);
    //MainWin.Color := getColor('status_bg',clInactiveBorder); not for scroll bars
    Status.Font.Color := getColor('status_font', ClInactiveCaptionText);
    htmlTheme.bg := Config.ReadValue('COLORS', 'html_bg', '#444444');
    htmlTheme.txt := Config.ReadValue('COLORS', 'html_font', '#eeeeee');
    htmlTheme.less := Config.ReadValue('COLORS', 'html_less', '#ff5555');
    htmlTheme.more := Config.ReadValue('COLORS', 'html_more', '#55ff55');


    libDir := GetCurrentDir + '/lib';
    if not FileExists(libDir + '/pacnews') { and FileExists(libSys + '/pacnews') } then
        libDir := libSys;

    Html.SetHtmlFromStr(getHtmlHeader + '<b>F10</b> ' + msg_info_save + '</body></html>');
    //journald_print(7,pchar('started'){,GetProcessID});
end;

procedure TMainWin.FormDestroy(Sender: TObject);
//var p: tpacnew;
begin
    //p := pacnews.Data[0];
    pacnews.erase();
    backupList.erase();
    FreeAndNil(pacnews);
    FreeAndNil(backupList);
    //writeln( #10'not removed ?? ',p.filename, p.level,p.caption); // !! not free !! but clear ?
    Config.writeValue('POSITION', 'h', self.Height);
    Config.writeValue('POSITION', 'w', self.Width);
    Config.writeValue('POSITION', 'x', self.Left);
    Config.writeValue('POSITION', 'y', self.Top);
    FreeAndNil(Config);
end;

procedure TMainWin.FormShow(Sender: TObject);
{const
    hookfile = '/etc/pacman.d/hooks/pacnew-chaser.hook'; }
var
    dir: String;
    //hook: Boolean; }
begin
    writeln('pacman hook: ', config.readValue('CONF', 'hook', 0));
    // find .pacnew
    PostMessage(Application.MainForm.Handle, APP_UPDATE, 1, 0);

    // create backup home directory if not exists
    dir := config.dirSave;

    if not controlBackupOk(False) then
        PostMessage(Application.MainForm.Handle, APP_BACKUP_OK, 1, 0);

    //desktopNotify('Hello');
{
    NEXT ? in parameters dialog ?
    sudo ln -s /usr/lib/pacnew-chaser/pacnew-chaser.hook /etc/pacman.d/hooks/pacnew-chaser.hook

    writeln('ini hook:', config.readValue('CONF','hook',0) );
    if config.readValue('CONF','hook',0) = 1 then
    begin
        writeln('create hook');
        if not FileExists(hookfile) then
            fpSymlink('/usr/lib/pacnew-chaser/pacnew-chaser.hook',hookfile);
    end
    else
    begin
        writeln('no hook');
        if FileExists(hookfile) then
            FpUnlink(hookfile);
    end;
}
end;

procedure TmainWin.WndProc(var Msg: TLMessage);
var
    Sender: TObject;
begin
    inherited WndProc(Msg);
    Sender := A_delete;
    if Msg.Wparam = 0 then
        Sender := self;
    case Msg.Msg of
        APP_UPDATE: A_updateExecute(Sender);
        APP_EDIT: EditFiles();
        APP_BACKUP_OK: controlBackupOk(Msg.WParam = 1);
    end;
end;

function TMainWin.getSeletedPacnew: Tpacnew;
var
    item: TListItem;
begin
    Result := nil;
    item := ListFiles.selected;
    if (item <> nil) then
        Result := self.pacnews.keyData[item.SubItems[0]];
end;

function TMainWin.getHtmlHeader: String;
    // bad css for table ?
begin
    Result := '<html>' + LineEnding + '<head> ' + LineEnding;
    Result += '<style>' + LineEnding;
    Result += 'td { color:#ff0000; }' + LineEnding;
    Result += 'tr { color:#00ff00; }' + LineEnding;
    Result += 'table { color:#0000ff; }' + LineEnding;
    Result += '</style>' + LineEnding;
    Result += '</head>' + LineEnding + '<body style="color:' + htmlTheme.txt + '; background-color:' + htmlTheme.bg + ';">' + LineEnding + '' + LineEnding;
end;

procedure TMainWin.ListFilesContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
var
    item: TListItem;
begin
    item := ListFiles.GetItemAt(MousePos.x, MousePos.y);
    Handled := True;
    if (item <> nil) {and ( not self.pacnews.keyData[item.SubItems[0]].notRead)} then
        PopupMenuItem.PopUp
    else
        PopupMenuApp.PopUp;
end;

procedure TMainWin.ListFilesDblClick(Sender: TObject);
var
    item: TListItem;
begin
    //item := ListFiles.GetItemAt(Mouse.CursorPos.x,Mouse.CursorPos.y);
    item := ListFiles.selected;
    if (item <> nil) then
    begin
        A_editExecute(Sender);
    end
    else
    begin
        //if (Editor.Lines.Count>1) then
        Splitter1.SetSplitterPosition(round(MainPanel.ClientHeight / 2));
    end;
end;

procedure TMainWin.ListFilesDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
    Accept := False;
end;



procedure TMainWin.ListFilesEditing(Sender: TObject; Item: TListItem; var AllowEdit: Boolean);
begin
    AllowEdit := False;
    //A_editExecute(Sender);
end;


procedure TMainWin.EditorDblClick(Sender: TObject);
begin
    Splitter1.SetSplitterPosition(round(MainPanel.ClientHeight / 2));
end;

procedure TMainWin.EditorDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
    //WriteLn('Editor.DragDrop');
    A_diffExecute(Sender);
end;

procedure TMainWin.EditorDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
    Accept := Sender = TListView(Sender);
    //  WriteLn('Editor.DragDrop');
    //Accept := true;
end;


procedure TMainWin.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
    A_closeExecute(Sender);
end;


procedure TMainWin.ListFilesUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
var
    item: TListItem;
begin
    item := ListFiles.selected;
    if item <> nil then
    begin
        if (UTF8Key = 'e') then
            A_editExecute(Sender);
        if (UTF8Key = 'i') then
            ShowMessage('info item: ' + item.Caption);
    end;
end;

procedure TMainWin.ListFilesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
{ VK_DELETE , VK_HELP, VK_F5}
var
    item: TListItem;
begin
    if (Key = VK_F5) then // F5 : 116
    begin
        A_updateExecute(Sender);
        exit;
        //ShowMessage(IntToStr(VK_F5));
    end;
    item := ListFiles.selected;
    if item <> nil then
    begin
        if (Key = VK_DELETE) then
            A_deleteExecute(Sender);
        if (Key = VK_HELP) or (Key = VK_F1) then
        begin
            A_manExecute(Sender);
        end;
    end;
end;

procedure TMainWin.ListFilesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
    item: TListItem;
begin
    item := ListFiles.GetItemAt(X, Y);
    if (item <> nil) then
    begin
        ListFiles.selected := Item;
        {if (clickleft) and (Button = mbLeft) then
        begin
            Application.ProcessMessages;
            PopupMenuItem.PopUp;
        end;}
        if (((ssShift in Shift) or (ssCtrl in Shift)) and not (ssDouble in Shift)) then
            A_diffExecute(Sender);
    end;
end;

procedure TMainWin.ListFilesSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
var
    p: Tpacnew;
begin
    //ShowMessage('Item: '+Item.Caption + '  -- sélectionné:'+BoolToStr(Selected));
    //Status.Panels[1].Text := 'sel: ' + BoolToStr(Selected);
    //Status.Panels[2].Text := Item.SubItems[1];
    if (Selected = False) then
    begin
        //Status.Panels[1].Text := '';
        //Status.Panels[2].Text := '';
        exit;
    end;
    if (ListFiles.selected <> nil) then
    begin
        p := self.pacnew;
        try
            Status.Panels[1].Text := p.diff;
            Status.Panels[2].Text := p.pacnew + '  ' + p.difference;
        except
            Status.Panels[1].Text := '-';
            Status.Panels[2].Text := p.pacnew;
            //on e: Exception do // cant read file
            //    Application.MessageBox(PChar('Error : ' + e.Message), 'Exception', 0);
        end;
    end
    else
    begin
        if (ListFiles.Items.Count = 1) then
        begin
            ListFiles.ItemIndex := 0;
            ListFiles.Items[0].Selected := True;
        end;
    end;
end;

function TMainWin.controlBackupOk(notify: Boolean = False): Boolean;
var
    dtfile:   TDateTime;
    long:     Integer;
    fileName: String;
begin
    Result := True;
    long := 0;
    fileName := Config.ReadValue('BACKUP', 'file', '');
    if (FileExists(fileName)) then
    begin
        dtfile := FileDateTodateTime(SysUtils.FileAge(fileName));
        long := DaysBetween(dtfile, now);
        if notify then
            writeln('global backup ', long, ' days old : ', filename);
    end;
    if (not FileExists(fileName) or (long > 4)) then
    begin
        if notify then
        begin
            desktopNotify(msg_msg_save, 6);
            writeln('global backup file no valid ' + fileName);
        end;
        Result := False;
    end;
end;

procedure TMainWin.A_editExecute(Sender: TObject);
var
    editModal: Boolean = False;
begin
    editModal := Config.ReadValue('EDITOR', 'modal', 1) = 1;
    if (editModal) then
    begin
        // noting ... html is to slow ??
        Html.SetHtmlFromStr(getHtmlHeader + msg_modalmode + '</body></html>');
        Caption := Caption + ' - ' + msg_modalmode;
        self.Enabled := False;
        Screen.Cursor := crHourGlass;
        //TODO
          {
          postMessage or write TTaskBash no modal...
          http://wiki.freepascal.org/Multithreaded_Application_Tutorial/fr
          http://wiki.freepascal.org/Asynchronous_Calls
          }
    end;
    PostMessage(Application.MainForm.Handle, APP_EDIT, 1, 0);
end;

{
 run by message APP_EDIT
}
procedure TMainWin.EditFiles();
var
    p:    Tpacnew;
    ed:   String;
    diff: Integer;
    editModal: Boolean = False;
begin
    p := self.pacnew;
    if p = nil then
        exit;

    ed := getEditor();
    if ed = '' then
    begin
        Application.MessageBox(PChar(msg_err_noeditor + sLineBreak + sLineBreak + 'diffuse, meld or kompare'), 'pacnew', $00000010);
        Screen.Cursor := crDefault;
        exit;
    end;

    //PostMessage(Application.MainForm.Handle, APP_BACKUP_OK, 1, 0);
    controlBackupOk();

    p.setMd5();
    if (p.backup) then
    begin
        desktopNotify(msg_backup_files + '\n' + Config.dirSave, 6);
    end;

    editModal := Config.ReadValue('EDITOR', 'modal', 1) = 1;
    with Tbash.Create() do
        try
            Execute(PChar('-r "' + p.fileName + '" "' + ed + '"'), editModal);
        finally
            Free;
        end;

    if (editModal) then
    begin
        self.Enabled := True;
        Caption := Tconfig.ProgramName;
        Screen.Cursor := crDefault;
        if (editModal) then
            Html.SetHtmlFromStr(getHtmlHeader + '</body></html>');
        diff := p.Md5Changed;
        //writeln('after modal edition: ',diff);

        if (diff = 2) then
        begin
            // only pacnew changed
            desktopNotify(msg_info_modified);
            ;
        end;
        if (diff = 1) then
        begin
            // only active file changed
            // OK
            desktopNotify(msg_info_file_modified);
            if p.fileIsAsPacnew then                          // test if md5 == old.pacnew
            begin
                //desktopNotify(msg_info_pacnew_toremove);
                A_deleteExecute(self);
            end;
        end;
        if (diff = -1) then
        begin
            // 2 files changed ??
            desktopNotify(msg_info_confuse);
            ;
        end;
    end;
    //onSelectFile(Sender);

end;

procedure TMainWin.A_toggleLevelExecute(Sender: TObject);
var
    p: Tpacnew;
begin
    p := self.pacnew;
    if p = nil then
        exit;

    if p.level = 0 then
        exit; // not red

    p.level := p.level + 1;
    if p.level > ImageListSmall.Count - 1 then
        p.level := 1;
    Config.writeValue('FILES', p.Caption, p.level);
    Config.merge();
    ListFiles.selected.ImageIndex := p.level;
end;

procedure TMainWin.A_toggleViewExecute(Sender: TObject);
begin
    if (ListFiles.ViewStyle = vsSmallIcon) then
        ListFiles.ViewStyle := vsList
    else
        ListFiles.ViewStyle := vsSmallIcon;
end;

procedure TMainWin.A_updateExecute(Sender: TObject);
var
    i:    Integer;
    item: TListItem;
    p:    Tpacnew;
    appProcess: Boolean = False;
begin
    appProcess := (Sender.ClassName <> 'TMainWin'); // for bug after delete .pacnew ?
    Screen.Cursor := crHourGlass;
    try
        Status.Panels[1].Text := msg_info_status;
        ListFiles.Items.Clear;
        {
        if backupList.scanFiles()>0 then
        begin
            try
            for i := 0 to backupList.Count - 1 do
            begin
                b :=backupList.Items[i];
                if not (b.modified and b.pacnew and not b.notRead) then
                   continue;
                writeln(i,b.ToString());
                item := ListFiles.Items.Add;
                p := Tpacnew.create( backupList.Items[i] );
                //writeln('bien créé')   ;
                writeln(p.ToString);
                pacnews.add(p.fileName,p);
                //writeln('bien ajouté')   ;
                //p := backupList.Items[i];
                p.level := Config.ReadValue('FILES', p.Caption, 1);
                item.Caption := p.Caption;
                item.ImageIndex := p.level;
                item.StateIndex := 0;
                item.SubItems.Add(p.fileName);
                writeln(p.ToString);
            end;

            except
                on e: Exception do
                   writeln{Application.MessageBox}(PChar('Error : ' +e.ClassName + ' '+e.Message +' '), 'Exception ', i);
            end;
            if (pacnews.Count = 1) then
            begin
                ListFiles.SelectAll;
                ListFiles.ItemIndex := 0;
            end;
            {$IFDEF LCLQT5}
            Status.Panels[0].Text := ' ' + IntToStr(pacnews.Count) + ' .pacnew';
           {$ENDIF}
           {$IFDEF LCLGTK3}
            MainWin.Caption := 'pacnew-chaser   ' + IntToStr(pacnews.Count) + ' .pacnew';
           {$ENDIF}
        end
        else
        begin
            desktopNotify(msg_no_pacnew, 5);
        end;
         }

        if pacnews.scanFiles(backupList, appProcess) > 0 then
        begin
            for i := 0 to pacnews.Count - 1 do
            begin
                item := ListFiles.Items.Add;
                p := pacnews.Data[i];
                p.level := Config.ReadValue('FILES', p.Caption, 1);
                item.Caption := p.Caption;
                item.ImageIndex := p.level;
                item.StateIndex := 0;
                item.SubItems.Add(p.fileName);
                //writeln(p.ToString);
            end;
            if (pacnews.Count = 1) then
            begin
                ListFiles.SelectAll;
                ListFiles.ItemIndex := 0;
            end;
           {$IFDEF LCLQT5}
            Status.Panels[0].Text := ' ' + IntToStr(pacnews.Count) + ' .pacnew';
           {$ENDIF}
           {$IFDEF LCLGTK3}
            MainWin.Caption := 'pacnew-chaser   ' + IntToStr(pacnews.Count) + ' .pacnew';
           {$ENDIF}
        end
        else
        begin
            desktopNotify(msg_no_pacnew, 5);
        end;

    finally
        Screen.Cursor := crDefault;
    end;
end;

procedure TMainWin.A_diffExecute(Sender: TObject);
var
    p: Tpacnew;
    i: Integer;
    s, attr: String;
begin
    Html.SetHtmlFromStr('');
    p := self.pacnew;
    if p <> nil then
    begin
        Screen.Cursor := crHourGlass;
        //TODO: How to show int (age) mouse over file in treeview ?
        attr := Config.ReadValue('OVERRIDE', p.Caption, '');
        if (attr <> '') then
        begin
            attr := ':: ' + msg_override + ' ' + attr + '<br>';
        end;
        s := '';
        s += '<i>--- ' + p.filename + ' (' + p.getAge(pacnewActif) + ')</i><br>';
        s += '<i>--- ' + p.pacnew + ' (' + p.getAge(pacnewFile) + ')</i><br><b>' + p.package + '</b><br>' + attr + '<br>';
        with Tbash.Create() do
            try
                if Execute(PChar('-d ' + p.fileName)) then
                begin
                    for i := 0 to output.Count - 1 do
                    begin
                        attr := htmlTheme.less;
                        if (copy(output[i], 1, 1) = '+') then
                            attr := htmlTheme.more;
                        s += '<span style="color:' + attr + '">' + output[i] + '</span><br>';
                    end;
                end;
                if (Html.ClientHeight < 60) then
                    Splitter1.SetSplitterPosition(round(MainPanel.ClientHeight / 2));
            finally
                Free;
            end;
        Html.SetHtmlFromStr(getHtmlHeader + s + '</body></html>');
        //writeln(s);
        Screen.Cursor := crDefault;
    end;
end;

procedure TMainWin.A_deleteExecute(Sender: TObject);
var
    p:     Tpacnew;
    Reply: Integer;
begin
    p := self.pacnew;
    if p = nil then
        exit;

    Reply := Application.MessageBox(PChar(msg_quest_remove + sLineBreak + p.pacnew), 'pacnew', MB_ICONQUESTION + MB_YESNO);
    if (Reply = idYes) then
    begin
        with Tbash.Create() do
            try
                command(PChar('pkexec /usr/bin/pacnew-chaser -a ' + p.fileName), True);
                if (not FileExists(p.Pacnew)) then
                begin
                    desktopNotify(p.Pacnew + ' ' + msg_info_removed);
                    { journalctl -t pacnew-chaser --no-pager }
                    journald_print(5, PChar('removed ' + p.Pacnew));
                    // do not send Application.ProcessMessages ???
                    PostMessage(Application.MainForm.Handle, APP_UPDATE, 0, 0);
                end
                else
                    writeln('Error: file not removed ! ', p.Pacnew);
            finally
                Free;
            end;
    end;

end;

procedure TMainWin.A_manExecute(Sender: TObject);
const
    fman: String = '/tmp/pacnew-man.html';
var
    p: Tpacnew;
    //fhtml: TStringList;
begin
    p := self.pacnew;
    if p = nil then
        exit;

    Screen.Cursor := crHourGlass;
    with Tbash.Create() do
        try
            if Execute(PChar('-h ' + p.Caption)) then
            begin
                if FileExists(fman) then
                begin
                    //fhtml := TStringList.Create;
                    //try
                    //fhtml.loadfromFile(fman);
                    //fhtml.text := ReplaceStr(fhtml.text,'<body>','<body style="background-color:'+htmlTheme.bg+'; color:'+htmlTheme.txt+'">');
                    //fhtml.text := ReplaceStr(fhtml.text,'<h2>','<h2 style="background-color:#44883c; color:#ffffff">');
                    Html.SetHtmlFromStr('');
                    Html.OpenURL(fman);  //   SetHtmlFromStream(fman);
                    //html.SetHtmlFromStr(fhtml.text);
                    //writeln(fhtml.text);
                    //finally
                    //   fhtml.free;
                    //end;

                    if Config.readValue('CONF', 'extern-man', 1) <> 0 then
                        OpenURL('file://' + fman);
                end;
            end;
        finally
            Free;
        end;
    Screen.Cursor := crDefault;

end;

procedure TMainWin.A_listConfsModidiedExecute(Sender: TObject);
var
    i:     Integer;
    s:     String;
    files: TList;
    b:     TBackup;
begin
    Screen.Cursor := crHourGlass;
    Html.SetHtmlFromStr('');
    writeln();
    s := '<i>--- ' + msg_html_modfiles + '</i><br><br>';

    files := backupList.getModifieds();
    try
        s += '<table>';
        for i := 0 to Pred(files.Count) do
        begin
            b := TBackup(files[i]);
            s += '<tr><td style="color:' + htmlTheme.txt + '">' + b.filename + ' </td><td style="color:' + htmlTheme.txt + '"> ' + b.package + ' </td></tr>';
            writeln(b.fileName);
        end;
        s += '</table>'
    finally
        files.Free;
    end;

    Html.SetHtmlFromStr(getHtmlHeader + s + '</body></html>');
    Screen.Cursor := crDefault;
    if (Html.ClientHeight < 60) then
        Splitter1.SetSplitterPosition(round(MainPanel.ClientHeight / 2));
end;

procedure TMainWin.A_listConfsExecute(Sender: TObject);
var
    i, idx: Integer;
    s:      String;
    list:   TStringList;
    b:      TBackup;
    stats:  TbackupSelect;
begin
    Screen.Cursor := crHourGlass;
    Html.SetHtmlFromStr('');
    writeln();
    s := '<i>--- ' + msg_html_backupfiles + '</i><br><br>';

    list := TStringList.Create();
    try
        for i := 0 to Pred(backupList.Count) do
        begin
            b := backupList[i];
            if b.modified then
                s += '<b style="color:' + htmlTheme.less + '">' + b.fileName + '</b><br>'
            else
                s += b.fileName + '<br>';
            writeln(b.fileName);
        end;
        stats := backupList.CountSelect();
        s += '<hr><ul>';
        s += '<li>' + IntToStr(stats.bsAll) + ' files</li>';
        s += '<li>' + IntToStr(stats.bsPackage) + ' packages</li>';
        s += '<li>' + IntToStr(stats.bsModified) + ' Modified files</li>';
        if stats.bsPacnew>0 then
           s += '<li>' + IntToStr(stats.bsPacnew) + ' .pacnew</li>';
        s += '</ul>';
        Html.SetHtmlFromStr(getHtmlHeader + s + '</body></html>');
    finally
        list.Free;
    end;

    Screen.Cursor := crDefault;
    if (Html.ClientHeight < 60) then
        Splitter1.SetSplitterPosition(round(MainPanel.ClientHeight / 2));

end;

procedure TMainWin.A_closeExecute(Sender: TObject);
begin
    if (FileExists(backupFile)) then
        deleteFile(backupFile);
    ListFiles.Items.Clear;
    if (Sender is TAction) then
        Application.Terminate;
    {
    genere un plantage
     while not Application.Terminated do
           Application.ProcessMessages;
    }
end;

procedure TMainWin.A_colorBgExecute(Sender: TObject);
begin
    colorDialog.Title := (Sender as TAction).Caption;// 'Select file color background';
    colorDialog.color := listFiles.color;
    if colorDialog.Execute then
    begin
        listFiles.color := colorDialog.Color;
    end;
end;

procedure TMainWin.A_colorTxtExecute(Sender: TObject);
begin
    colorDialog.Title := (Sender as TAction).Caption;//'Select file text color';
    colorDialog.color := listFiles.font.color;
    if colorDialog.Execute then
    begin
        listFiles.font.color := colorDialog.Color;
    end;
end;

procedure TMainWin.A_backupExecute(Sender: TObject);
begin
    Screen.Cursor := crHourGlass;
    with Tbash.Create() do
        try
            if Execute(PChar('--backup')) then
            begin
                if (FileExists(backupFile)) then
                begin
                    Screen.Cursor := crDefault;
                    SaveDialog.FileName := Config.dirSave + '/' + ExtractFileName(backupFile);
                    if (SaveDialog.Execute) then
                    begin
                        CopyFile(backupFile, SaveDialog.Filename);
                        Config.writeValue('BACKUP', 'file', SaveDialog.Filename);
                    end;
                end;
            end;
        finally
            Free;
        end;
    Screen.Cursor := crDefault;
end;


function TMainWin.getEditor: String;
var
    eds: array [0..2] of String = ('/usr/bin/diffuse', '/usr/bin/meld', '/usr/bin/kompare');
    ed:  String;
begin
    Result := Config.ReadValue('EDITOR', 'edit', '0');
    // editor in ini file can have somes parameters
    if FileExists(Result.Split(' ')[0]) then
        exit;
    Result := '';
    for ed in eds do
    begin
        if FileExists(ed) then
        begin
            Result := ed;
            break;
        end;
    end;
end;


procedure TMainWin.PopupMenuItemPopup(Sender: TObject);
var
    p: Tpacnew;
begin
    p := self.pacnew;
    if p <> nil then
    begin
        M_diff.Enabled := not p.notRead;
        try
            M_diff.Caption := msg_mnu_diff + ' ' + p.diff;
        except
            ;
            //on e: Exception do
            //Application.MessageBox(PChar('Error : ' + e.Message), 'Exception', 0);
        end;
    end;
end;

procedure TMainWin.desktopNotify(message: String; long: Integer = 3);
// notify-send "$message" -t 3000 -a pacnew-chaser --icon=pacnew-chaser -u low
begin
    with Tbash.Create() do
        try
            Execute(PChar('-n "' + message + '" ' + IntToStr(long * 1000)), False);
        finally
            Free;
        end;
end;

end.
