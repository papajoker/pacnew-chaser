unit pacnew;

{$mode objfpc}{$H+}


interface

uses
    Classes, SysUtils, fgl, Config, md5, backups;

const
    pacnewActif = 1;
    pacnewFile = 2;

type

    TpacnewDiff = record
        more: Integer;
        less: Integer;
    end;

    TpacnewMd5 = record
        afile: TMD5Digest;
        pacnew: TMD5Digest;
    end;

    Tage = record
        afile: TDateTime;
        pacnew: TDateTime;
    end;

    TPacnew = class(TObject)
    strict private
        ffile: String;
        fpackage: String;
        flevel: Integer;
        fdiff: TpacnewDiff;
        fage: Tage;
        fmd5: TpacnewMd5;
        fIsText: Boolean;
        fnotRead: Boolean;
        function getCaption: String;
        function getPacnew: String;
        function getLevel: Integer;
        function getDiff: String;
        procedure setDiff(adiff: String);
    public
        constructor Create(afile: String); overload;
        constructor Create(fileBackup: Tbackup); overload;
        function difference: String;
        function ToString: Ansistring; override;
        procedure setMd5;
        function Md5Changed: Integer;
        function fileIsAsPacnew(): Boolean;
        class procedure gitCommit(await: Boolean = True); static;
        function backup: Boolean;
        function getAge(index: Integer): String;
        procedure inject(fileBackup: Tbackup);
    published
        property Caption: String read getCaption;
        property fileName: String read ffile;
        property package: String read fpackage;
        property pacnew: String read getPacnew;
        property level: Integer read getLevel write flevel;
        property diff: String read getDiff write setDiff;
        property AgeFile: String index pacnewActif read getAge;
        property AgePacnew: String index pacnewFile read getAge;
        property istext: Boolean read fIsText;
        property notRead: Boolean read fnotRead;

    end;

    TPacnews = specialize TFPGMap<String, Tpacnew>;

    TPacnewsHelper = class helper for TPacnews
        function scanFiles(BackupList: TBackupList; appProcess: Boolean = False): Integer;
        procedure erase();     // usefull ???? include in free ??
    end;

{
 Tpacnews.add('/etc/pacman.conf', new Tpacnew;create('/etc/pacman.conf') )
 i := TPacnews.IndexOf('/etc/pacman.conf')
 if TPacnews.find('/etc/pacman.conf',i) then
 //p := TPacnews['/etc/pacman.conf']
 p := TPacnews.keyData['/etc/pacman.conf']
 p := TPacnews.data[id]
}

    // not used
    TPacnewList = specialize TFPGObjectList<TPacnew>;

    TPacnewListHelper = class helper for TPacnewList
        function IndexOf(filename: String): Integer; overload;
        function findByFile(fileName: String): Tpacnew;
    end;

implementation

uses
    bash, fileutil, baseUnix;

constructor TPacnew.Create(afile: String);
{var s : stat;
    attr: Longint; }
begin
    inherited Create();
    if (not fileExists(afile)) then
        raise Exception.Create('File not exist');

    ffile := afile;
    flevel := -1;
    fnotRead := False;
    with fdiff do
    begin
        more := -1; // force call to script
        less := +1;
    end;
    fage.afile := FileDateTodateTime(SysUtils.FileAge(afile));
    fage.pacnew := FileDateTodateTime(SysUtils.FileAge(afile + '.pacnew'));

    {
    // have [ -r "file" ] in bash
    fReadWrite := True;
    fIsText := True;
    attr := FileGetAttr(afile);
    if attr = -1 then
        fReadWrite := False;
        }
    {
    baseUnix.fpstat(afile, s);
    writeln(self.caption+' FileGetAttr :',attr, fReadWrite);
    If (attr and faReadOnly)<>0 then
    begin
      Writeln ('File is ReadOnly',attr and faReadOnly);
    end;
    If (attr and faSysFile)<>0 then
      Writeln ('File is a system file',attr and faSysFile);
      }

end;

constructor TPacnew.Create(fileBackup: Tbackup);
begin
    Create(fileBackup.fileName);
    inject(fileBackup);
end;

procedure TPacnew.inject(fileBackup: Tbackup);
begin
    if (ffile <> fileBackup.filename) then
    begin
        ffile := fileBackup.filename;
        fage.afile := FileDateTodateTime(SysUtils.FileAge(ffile));
        fage.pacnew := FileDateTodateTime(SysUtils.FileAge(ffile + '.pacnew'));
    end;
    fnotRead := fileBackup.notRead;
    fpackage := fileBackup.package;
end;

function TPacnew.getCaption: String;
begin
    Result := ExtractFileName(ffile);
end;

function TPacnew.getPacnew: String;
begin
    Result := ffile + '.pacnew';
end;

function TPacnew.getLevel: Integer;
{var
  ini: TiniFile; }
begin
  {if (flevel<0) then
  begin
    ini := TINIFile.Create(iniFile);
    try
       flevel := ini.ReadInteger('FILES', self.Caption, 0);
    finally
       ini.free;
    end;
  end; }
    Result := flevel;
end;

procedure TPacnew.setDiff(adiff: String);
var
    ss: array of String;
begin
    adiff := adiff.Trim;
    ss := adiff.trim.Split(' ');
    try
        fdiff.less := StrToInt(ss[0]);
        fdiff.more := StrToInt(ss[1]);
    except
        raise Exception.Create('diff bad format: ' + adiff);
    end;
end;

function TPacnew.getDiff: String;
begin
    if (fdiff.more < 0) then
    begin
        // value not assigned ! run script bash for first get
        with Tbash.Create() do
            try
                if Execute(PChar('-c ' + self.fileName)) then
                begin
                    self.setDiff(output.Text);
                end
                else
                begin
                    self.fnotRead := False;
                    raise Exception.Create('Difference script bad return');
                end;
            finally
                Free;
            end;
    end;
    Result := '';
    if (fdiff.less < 0) then
        Result := IntToStr(fdiff.less);
    if (fdiff.more > 0) then
        Result += ' +' + IntToStr(fdiff.more) + ' ';
end;

function TPacnew.difference: String;
var
    plus: String;
begin
    self.getdiff();
    Result := '';
    if (fdiff.less < 0) then
    begin
        plus := '';
        if (fdiff.less < -1) then
            plus := 's';
        Result := IntToStr(fdiff.less) + ' line' + plus;
    end;
    if (fdiff.more > 0) then
    begin
        plus := '';
        if (fdiff.more > 1) then
            plus := 's';
        Result += ' +' + IntToStr(fdiff.more) + ' line' + plus;
    end;
end;

function Tpacnew.ToString: Ansistring;
begin
    Result := self.ffile + ' (' + IntToStr(self.level) + ') ';// +self.diff+ '= '+self.difference;
end;

function Tpacnew.getAge(index: Integer): String;
var
    a: TDateTime;
begin
    if (index = pacnewActif) then
        a := self.fage.afile
    else
        a := self.fage.pacnew;
    Result := DateToStr(a);
    //result := FormatDateTime('dddddd',a);
end;

procedure Tpacnew.setMd5;
begin
    fmd5.afile := MD5File(fileName);
    fmd5.pacnew := MD5File(fileName + '.pacnew');
end;

{
 if in editor, user copy all pacnew in active conf file
}
function Tpacnew.fileIsAsPacnew(): Boolean;
begin
    Result := MDMatch(MD5File(fileName), fmd5.pacnew);
end;


function Tpacnew.Md5Changed: Integer;
var
    f, p: TMD5Digest;
begin
    Result := 0;
    f := MD5File(fileName);
    p := MD5File(fileName + '.pacnew');
    if not MDMatch(f, fmd5.afile) then
        Result := 1;
    if not MDMatch(p, fmd5.pacnew) then
        Result := 2;
    if (Result > 0) and (not MDMatch(f, fmd5.afile)) and (not MDMatch(p, fmd5.pacnew)) then
        Result := -1;
end;

class procedure Tpacnew.gitCommit(await: Boolean = True); static;
// git --git-dir="$HOME/.local/share/pacnew-chaser/.git" --work-tree="$HOME/.local/share/pacnew-chaser" --no-pager diff
var
    gitCommand: String = 'git add *.back; git commit -am "';
begin
    with Tbash.Create() do
        try
            command(PChar('cd "' + Tconfig.dirSave + '";' + gitCommand + DateTimeToStr(Now) + '"'), await);
        finally
            Free;
        end;
end;

function Tpacnew.backup: Boolean;
var
    dir: String;
begin
    Result := False;
    dir := Tconfig.dirSave;
    gitCommit(True);
    Result := CopyFile(self.fileName, dir + self.fileName + '.back', [cffOverwriteFile, cffCreateDestDirectory, cffPreserveTime]);
    if Result then
        Result := CopyFile(self.fileName + '.pacnew', dir + self.fileName + '.pacnew.back', [cffOverwriteFile, cffCreateDestDirectory, cffPreserveTime]);
    if Result then; //gitCommit();
end;

// -------------------------------- //

function TPacnewsHelper.scanFiles(BackupList: TBackupList; appProcess: Boolean = False): Integer;
var
    p: Tpacnew;
    i: Integer;
    b: Tbackup;
begin
    self.erase();
    if backupList.scanFiles(appProcess) > 0 then
    begin
        for i := 0 to backupList.Count - 1 do
        begin
            b := backupList.Items[i];
            if not (b.modified and b.pacnew {and not b.notRead}) then
                continue;
            writeln(i, b.ToString());
            p := Tpacnew.Create(backupList.Items[i]);
            writeln(p.ToString);
            self.add(p.fileName, p);
            //p.level := Config.ReadValue('FILES', p.Caption, 1);
            writeln(p.ToString);
        end;
    end;
    {with Tbash.Create() do
        try
            if Execute('-l') then
            begin
                for i := 0 to output.Count - 1 do
                    if fileExists(output[i]) then
                    begin
                        p := Tpacnew.Create(output[i]);
                        self.add(p.fileName, p);
                    end;
            end;
        finally
            Free;
        end;}
    Result := self.Count;
end;

procedure TPacnewsHelper.erase();
var
    i: Integer;
    p: Tpacnew;
begin
    try
        for i := 0 to Pred(self.Count) do
        begin
            p := self.Data[i];
            //writeln('TPacnewsHelper.erase: ',p.caption, ' ', i, '/', self.Count);
            FreeAndNil(p); // exception ??
        end;
    except
        on e: Exception do
            writeln(PChar('Error : ' + e.ClassName + ' : ' + e.Message), 'Exception', i, '/', self.Count);
    end;
    Clear();
end;

// -------------------------------- //


function TPacnewListHelper.IndexOf(filename: String): Integer; overload;
var
    i:    Integer;
    item: TPacnew;
begin
    Result := -1;
    for i := 0 to Pred(Self.Count) do
    begin
        item := Self[i];
        if CompareText(item.filename, filename) = 0 then
        begin
            Exit(i);
        end;
    end;
    raise Exception.Create('pacnew not found in list');
end;

function TPacnewListHelper.findByFile(fileName: String): Tpacnew;
var
    i: Integer;
begin
    Result := nil;
    i := self.IndexOf(filename);
    if (i > -1) then
        Result := self.Items[i];
end;



end.
