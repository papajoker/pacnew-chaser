unit backups;

{$mode objfpc}{$H+}{$J-}
{$modeSwitch advancedRecords}

interface

uses
    Classes,
    Forms,
    SysUtils,
    fgl,
    md5;

{
for test clear cache:
sudo bash -c "sync; echo 3 >/proc/sys/vm/drop_caches"
}

const
    LOCAL_DB_PATH = '/var/lib/pacman/local';

type

    TbackupSelect = record
        bsAll,
        bsModified,
        bsPacnew,
        bsPackagesModified,
        bsPackage: Integer
    end;

    TBackup = class
        filename: String;
        package: String;
        md5: String;
        modified: Boolean;
        pacnew: Boolean;
        notRead: Boolean;
    private
        function md5Ok(): Boolean;
        procedure parseLine(line: String);
        function getPackageName(pathFile: String): String;
    public
        function ToString(): Ansistring; override;
        function outString(): String;
        constructor Create(line: String; pathFile: String);
    end;

    TBackupList = specialize TFPGList<TBackup>;

    TBackupListHelper = class helper for TBackupList
        function scanFiles(appProcess: Boolean = False): Integer;
        function readFile(fileName: String): Integer;
        function getModifieds(): TList;
        function getPacnews(): TList;
        procedure erase();
        function countSelect(): TbackupSelect;
    end;

implementation

constructor TBackup.Create(line: String; pathFile: String);
var
    textFile: TFileStream = nil;
begin
    filename := '';
    md5 := '';
    modified := False;
    pacnew := False;
    notRead := False;
    parseLine(line);
    package := self.getPackageName(pathFile);

    if pacnew then
    begin
        try
            try
                textFile := TFileStream.Create(fileName, fmOpenRead);
                notRead := False;
            except
                On E: EFOpenError do
                begin
                    notRead := True;
                    //writeln(E.ClassName + ' : ' + E.Message + ' for ' + filename);
                end;
            end;
        finally
            if textFile <> nil then
                textFile.Free;
        end;
    end;
end;

function TBackup.getPackageName(pathFile: String): String;
var
    ss:  array of String;
    dir: String;
    i:   Integer;
begin
    Result := '';
    //get last directory
    ss := pathFile.trim.Split('/');
    dir := ss[High(ss) - 1];
    //dir to package
    ss := dir.trim.Split('-');
    //remove les 2 derniers
    for i := 0 to High(ss) - 2 do
        Result += '-' + ss[i];
    Result := Result.Substring(1);
end;

function TBackup.md5Ok(): Boolean;
begin
    Result := (CompareStr(md5, MDPrint(MD5File(fileName))) <> 0);
end;


procedure TBackup.parseLine(line: String);
var
    ss: array of String;
begin
    ss := line.trim.Split(#9);
    try
        self.filename := '/' + ss[0].trim();
        self.md5 := ss[1].trim();
        self.pacnew := FileExists(filename + '.pacnew');
        //        if pacnew then // test if no .pacnew ? not
        self.modified := md5Ok();
    except
        raise Exception.Create('pacman base bad format: ' + line);
    end;
end;


function TBackup.ToString(): Ansistring;
const
    s = '                                                                 ';
var
    l: Integer;
begin
    Result := '';
    if notRead then
        Result := '**';
    l := 32 - length(filename + Result);
    Result := filename + copy(s, 1, l) + Result + #9;
    if not modified then
        Result += 'UN';
    Result += 'MODIFIED ';
    if modified then
        Result += '  ';
    if pacnew then
        Result += 'PACNEW '
    else
        Result += '       ';
    Result += md5;
    if pacnew then
        Result += '   ' + copy(MDPrint(MD5File(fileName)), 1, 4) + '...';
    Result += ' ' + package;
end;

function TBackup.outString(): String;
begin
    system.Write(filename: 32);
    Result := '  ';
    if not modified then
        Result += 'UN';
    Result += 'MODIFIED ';
    if modified then
        Result += '  ';
    if pacnew then
        Result += 'PACNEW '
    else
        Result += '       ';
    Result += md5;
    if pacnew then
        Result += '   ' + copy(MDPrint(MD5File(fileName)), 1, 4) + '...';
    writeln(Result);
    Result := filename + #9 + Result;
end;

function TBackupListHelper.readFile(fileName: String): Integer;
var
    nb:     Integer;
    filein: Text;
    line:   String;
    found:  Boolean = False;
begin
    found := False;
    nb := self.Count;
    Result := 0;
    System.Assign(filein, fileName);
    try
        reset(filein);
        while not EOF(filein) do
        begin
            Readln(filein, line);
            if (found) then
            begin
                if (line = '') then
                begin
                    found := False;
                end
                else
                begin
                    //                    writeln('readfile add: ',line);
                    self.Add(Tbackup.Create(line, fileName));
                    //                    writeln('readfile added ');
                    //writeln(backup.tostring);
                end;
            end;
            if (line = '%BACKUP%') then
                found := True;
        end;
    finally
        Close(filein);
    end;
    Result := self.Count - nb;
end;

function compareBackups(const Item1, Item2: TBackup): Integer;
begin
    Result := SysUtils.CompareText(item1.fileName, item2.fileName);
end;

function TBackupListHelper.scanFiles(appProcess: Boolean = False): Integer;
var
    path: String;
    info: TSearchRec;
begin
    erase();
    if FindFirst(LOCAL_DB_PATH + '/*.*', faDirectory, Info) = 0 then
    begin
        repeat
            path := LOCAL_DB_PATH + '/' + Info.Name + '/files';
            if not FileExists(path) then
                continue;
            if appProcess then
            begin
                {
                BUG ??
                only after delete .pacnew, ProcessMessages wait 30 seconds ??
                //writeln('appProcess :',appProcess);
                }
                Application.ProcessMessages;
            end;
            readFile(path);
        until findNext(Info) <> 0;
    end;
    FindClose(Info);
    sort(@compareBackups);
    Result := self.Count;
end;

function TBackupListHelper.getModifieds(): TList;
var
    i: Integer;
begin
    Result := TList.Create();
    for i := 0 to Pred(self.Count) do
    begin
        if self.Items[i].modified then
            Result.Add(self.Items[i]);
    end;
end;

function TBackupListHelper.getPacnews(): TList;
var
    i: Integer;
begin
    Result := TList.Create();
    for i := 0 to Pred(self.Count) do
    begin
        if self.Items[i].pacnew then
            Result.Add(self.Items[i]);
    end;
end;

function TBackupListHelper.countSelect(): TbackupSelect;
var
    i: Integer;
    b: Tbackup;
    packages: TStringList;
begin

    Result.bsAll := self.Count;
    Result.bsPacnew := 0;
    Result.bsModified := 0;
    Result.bsPackage := 0;
    packages := TStringList.Create;
    try
        for i := 0 to Pred(self.Count) do
        begin
            b := self.Items[i];
            if b.pacnew then
                Result.bsPacnew += 1;
            if b.modified then
                Result.bsModified += 1;
            if packages.IndexOf(b.package) < 0 then
                packages.add(b.package);
        end;
        Result.bsPackage := packages.Count;
    finally
        packages.Free;
    end;
end;

procedure TBackupListHelper.erase();
var
    i: Integer;
    b: Tbackup;
begin
    //writeln('TBackupListHelper.erase');
    try
        if self.Count > 0 then
            for i := 0 to Pred(self.Count) do
            begin
                //writeln('TBackupListHelper.erase', i, '/', self.Count);
                b := TBackup(self.Items[i]);
                FreeAndNil(b);
            end;
    except
        on e: Exception do
            writeln(PChar('Error : ' + e.Message), 'Exception', 0);
    end;
    //writeln('TBackupListHelper.erase', 'clear');
    Clear();
end;

end.
