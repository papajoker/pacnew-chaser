unit config;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, IniFiles, bash;

type

    TConfig = class
    private
    const
        cPprogramName = 'pacnew-chaser';
        class var fProgramName: String;
        iniR, iniW: TIniFile;
        class function getIniEtcFile: String; static;
        class function getIniHomeFile: String; static;
        class function getInitmpFile(): String; static;
        class function getDirSave(): String; static;
    public
    const
        version = '0.9.22';
        ProgramName = 'pacnew-chaser';
    var
        toto: String;
        class constructor Create;
        class property IniEtcFile: String read getIniEtcFile;
        class property IniHomeFile: String read getIniHomeFile;
        class property InitmpFile: String read getInitmpFile;
        class property dirSave: String read getDirSave;

        constructor Create();
        destructor Destroy(); override;

        function ReadValue(section: String; key: String; default: Integer = 0): Integer;
        function ReadValue(section: String; key: String; default: String = ''): String; overload;
        procedure writeValue(section: String; key: String; Value: Integer);
        procedure writeValue(section: String; key: String; Value: String); overload;
        procedure merge();
    end;


implementation

class constructor TConfig.Create;
begin
    fProgramName := cPprogramName;
end;

class function TConfig.getIniEtcFile(): String;
begin
    Result := '/etc/' + ProgramName + '.ini';
end;

class function TConfig.getIniHomeFile(): String;
begin
    Result := GetEnvironmentVariable('HOME') + '/.config/' + ProgramName + '.ini';
end;

class function TConfig.getInitmpFile(): String;
begin
    Result := '/tmp/' + ProgramName + '.ini';
end;

class function TConfig.getDirSave(): String;
var
    gitCommand: String = 'git init ';
begin
    Result := GetEnvironmentVariable('HOME') + '/.local/share/' + ProgramName;
    if not FileExists(Result) then
    begin
        if not ForceDirectories(Result) then
            raise Exception.Create('mkdir: ' + Result);
    end;
    if not FileExists(Result + '/.git') then
        with Tbash.Create() do
            try
                command(PChar(gitCommand + '"' + Result + '"'), False);
            finally
                Free;
            end;
end;

class procedure MergeIniFiles(const FromFilename, ToFilename, OutputFilename: String; const Overwrite: Boolean = True);
var
    IniFrom, IniTo, IniOut: TIniFile;
    Sec:     TStringList;
    Val:     TStringList;
    X, Y:    Integer;
    S, N, V: String;
begin
    if FileExists(OutputFilename) then
        DeleteFile(OutputFilename);
    IniFrom := TIniFile.Create(FromFilename);
    IniTo := TIniFile.Create(ToFilename);
    IniOut := TIniFile.Create(OutputFilename);
    Sec := TStringList.Create;
    Val := TStringList.Create;
    try
        IniFrom.ReadSections(Sec);
        for X := 0 to Sec.Count - 1 do
        begin
            S := Sec[X];
            IniFrom.ReadSection(S, Val);
            for Y := 0 to Val.Count - 1 do
            begin
                N := Val[Y];
                V := IniFrom.ReadString(S, N, '');
                IniOut.WriteString(S, N, V);
            end;
        end;

        IniTo.ReadSections(Sec);
        for X := 0 to Sec.Count - 1 do
        begin
            S := Sec[X];
            IniTo.ReadSection(S, Val);
            for Y := 0 to Val.Count - 1 do
            begin
                N := Val[Y];
                V := IniTo.ReadString(S, N, '');
                if Overwrite then
                begin
                    IniOut.WriteString(S, N, V);
                end
                else
                begin
                    if not IniOut.ValueExists(S, N) then
                        IniOut.WriteString(S, N, V);
                end;
            end;
        end;
    finally
        Val.Free;
        Sec.Free;
        IniOut.Free;
        IniTo.Free;
        IniFrom.Free;
    end;
end;

constructor TConfig.Create();
begin
    if not FileExists(IniHomeFile) then
        FileCreate(IniHomeFile);
    merge();
    iniW := TIniFile.Create(IniHomeFile);
end;

destructor TConfig.Destroy();
begin
    FreeAndNil(iniR);
    FreeAndNil(iniW);
    if FileExists(InitmpFile) then
        DeleteFile(InitmpFile);
end;

procedure Tconfig.merge();
begin
    FreeAndNil(iniR);
    MergeIniFiles(IniEtcFile, IniHomeFile, InitmpFile, True);
    iniR := TIniFile.Create(InitmpFile);
end;

function TConfig.ReadValue(section: String; key: String; default: Integer = 0): Integer;
begin
    Result := iniR.ReadInteger(section, key, default);
end;

procedure TConfig.writeValue(section: String; key: String; Value: Integer);
begin
    iniW.WriteInteger(section, key, Value);
end;

function TConfig.ReadValue(section: String; key: String; default: String = ''): String;
begin
    Result := iniR.ReadString(section, key, default);
end;

procedure TConfig.writeValue(section: String; key: String; Value: String);
begin
    iniW.WriteString(section, key, Value);
end;

end.
