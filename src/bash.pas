unit bash;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Process;

const
    libSystem = '/usr/lib/pacnew-chaser';
    bashscript = 'pacnews';

type

    Tbash = class
    private
        fwaitExit: Boolean;
        foutput: TStringList;
        foptions: TProcessOptions;
        flibDir: String;
        procedure setWaitExit(Value: Boolean);
        function fexecute(command: PChar; lib: Boolean = True; waitExit: Boolean = True): Boolean;
    public
        constructor Create();
        destructor Destroy(); override;
        function Execute(command: PChar; waitExit: Boolean = True): Boolean;
        function command(toRun: PChar; waitExit: Boolean = True): Boolean;
    published
        property wait: Boolean read fwaitExit write setWaitExit;
        property output: TStringList read foutput;
    end;

implementation

uses UTF8Process;

constructor Tbash.Create();
begin
    inherited;
    fwaitExit := True;
    foutput := nil;
    flibDir := GetCurrentDir + '/lib';
    if not FileExists(flibDir + '/' + bashscript) then
        flibDir := libSystem
    else
        // only for developer, test "local" bash library
        writeln('::dev lib used ! ' + LineEnding + flibDir + '/' + bashscript);

end;

destructor Tbash.Destroy();
begin
    if assigned(foutput) then
        foutput.Free;
    inherited;
end;

procedure Tbash.setWaitExit(Value: Boolean);
begin
    fwaitExit := Value;
    if (fwaitExit) then
    begin
        foptions := [poWaitOnExit, poUsePipes, poStderrToOutPut];
        if assigned(foutput) then
            foutput.Clear
        else
            foutput := TStringList.Create();
    end
    else
    begin
        FreeAndNil(foutput);
        foptions := [poUsePipes];
    end;
end;

function Tbash.fexecute(command: PChar; lib: Boolean = True; waitExit: Boolean = True): Boolean;
var
    Process: TProcessUTF8;
    runlib:  String = '';
begin
    self.wait := waitExit;
    Result := False;
    Process := TProcessUTF8.Create(nil);
    try
        Process.Options := Process.Options + foptions;
        Process.Executable := '/usr/bin/bash';
        Process.Parameters.add('-c');
        if (lib) then
            runlib := flibDir + '/' + bashscript + ' ';
        Process.Parameters.add(runLib + command);
        Process.Execute;

        if (Process.ExitCode = 0) then
        begin
            Result := True;
        end
        else
        begin
            if Assigned(foutput) then
            begin
                foutput.Add('ExitStatus:' + IntToStr(Process.ExitStatus));
                foutput.Add('ExitCode:' + IntToStr(Process.ExitCode));
            end;
        end;
        if Assigned(foutput) then
            foutput.LoadFromStream(Process.Output);
    finally
        Process.Free;
    end;
end;

function Tbash.Execute(command: PChar; waitExit: Boolean = True): Boolean;
begin
    Result := fexecute(command, True, waitExit);
end;

function Tbash.command(toRun: PChar; waitExit: Boolean = True): Boolean;
begin
    Result := fexecute(toRun, False, waitExit);
end;

end.
