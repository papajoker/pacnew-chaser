program pacnewsgui;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
    cthreads, {$ENDIF} {$ENDIF}
    Interfaces, // this includes the LCL widgetset
    Forms,
    main { you can add units after this },
    DefaultTranslator,
    SysUtils,
    Process {, BaseUnix};

{$R *.res}

    procedure getAppParams();
    var
        i: Integer;
    begin
        //WriteLn('nb Params: ',IntToStr(ParamCount));
        for i := 1 to ParamCount do
        begin
            //WriteLn('param: ',ParamStr(i));
            if (ParamStr(i) = '--clickleft') then
            begin
                ;
                //clickLeft := true;
                //WriteLn('clickLeft: OK');
            end;
        end;
    end;

    function runScript(command: PChar): Boolean;
    begin
        Result := False;
        with TProcess.Create(nil) do
            try
                CommandLine := command;
                Execute;
            finally
                Free;
            end;
    end;

begin
    //http://wiki.freepascal.org/Command_line_parameters_and_environment_variables
    //WriteLn('ParamCount: ',IntToStr(ParamCount));
    if (ParamCount > 1) then
    begin
        if (ParamStr(1) = '-a') then
        begin
            if (FileExists(ParamStr(2) + '.pacnew')) then
                DeleteFile(ParamStr(2) + '.pacnew');
            exit;
        end;
    end;
    //getAppParams();


    Application.Title := 'pacnew-chaser';
    RequireDerivedFormResource := True;
    Application.Initialize;
    Application.CreateForm(TMainWin, MainWin);
    Application.Run;

  {
   bug if XDG_CURRENT_DESKTOP == KDE !!!
  }

    //Writeln ('desktop session: '+ GetEnvironmentVariable('DESKTOP_SESSION'));
    if (GetEnvironmentVariable('XDG_CURRENT_DESKTOP') = 'KDE') then
        runScript(PChar('kill -15 ' + IntToStr(GetProcessID)))
    else
        Writeln('not KDE :) ->' + ExtractFileName(GetEnvironmentVariable('DESKTOP_SESSION')));

end.


