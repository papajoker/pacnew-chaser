# pacnew-chaser

![](https://forum.manjaro.org/uploads/default/original/3X/5/2/52b709efad49d4221ad1cb618c828fdadeea8f32.jpg)

## Installation
With AUR :

    yaourt -S pacnew-chaser
    
## Usage

manage .pacnew files, compare for merge (qt gui) 

## Configuration

in `/etc/pacnew-chaser.ini` file
can overload with `~/.config/pacnew-chaser.ini`
